function toggleMenu() {
    const menuLayer = document.getElementById('menu-layer');
    menuLayer.classList.toggle('active');
}

document.querySelectorAll('.menu-item').forEach(item => {
    item.addEventListener('click', function() {
        const menuLayer = document.getElementById('menu-layer');
        menuLayer.classList.remove('active');
    });
});
