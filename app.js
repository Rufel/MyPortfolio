function toggleMenu() {
    const menuLayer = document.getElementById('menu-layer');
    menuLayer.classList.toggle('active');
}

document.querySelectorAll('.menu-item').forEach(item => {
    item.addEventListener('click', function() {
        const menuLayer = document.getElementById('menu-layer');
        menuLayer.classList.remove('active');
    });
});

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

function toggleIntroduction() {
    const introduction = document.getElementById('introduction');
    introduction.classList.toggle('active');
}
function toggleIntroduction() {
    const introductionSection = document.getElementById('introduction');
    const toggleButton = document.getElementById('toggle-button');
    const hiddenContent = document.querySelector('.hidden-content');

    if (introductionSection.classList.contains('active')) {
        toggleButton.textContent = 'Introduction';
        introductionSection.classList.remove('active');
    } else {
        toggleButton.textContent = 'Introduction';
        introductionSection.classList.add('active');
    }
}
